#!/bin/bash

# Substitute this extermely complex calculation
# with the one that best suits your backend
#
# Input arguments:
# $1 Signed GET URL
# $2 Checksum Type (md5, adler32, crc32)
#
# Output:
# Should stdout with the format (no quotations):
# ">>>>> HASH 123124\n"

API='localhost:8000'
URL=$1
HASH_TYPE=$2

checksum=$(curl -k -XPOST $API/$HASH_TYPE --data-urlencode url=$URL)
#Strip leading and trailing quotes.
checksum=$(sed -e 's/^"//' -e 's/"$//' <<<"$checksum")

echo -e ">>>>> HASH $checksum\n"
