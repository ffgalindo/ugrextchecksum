#!/bin/python3.4
# Used to test getting the checksum of file ztest. Only works if there is one
# endpoint defined in the ednpoints.conf file.

import sys

from dynafed_storagestats import configloader
import boto3
import botocore.vendored.requests.exceptions as botoRequestsExceptions
import botocore.exceptions as botoExceptions
from botocore.client import Config
import requests
from requests_aws4auth import AWS4Auth
from pprint import pprint
config_path = ['/etc/ugr/conf.d/endpoints.conf']
storage_shares = configloader.get_storage_shares(config_path)
storage_share = storage_shares[0]

if storage_share.plugin_settings['s3.alternate'].lower() == 'true' or storage_share.plugin_settings['s3.alternate'].lower() == 'yes':
    _api_url = '{scheme}://{netloc}'.format(
        scheme=storage_share.uri['scheme'],
        netloc=storage_share.uri['netloc']
    )
else:
    _api_url = '{scheme}://{domain}'.format(
        scheme=storage_share.uri['scheme'],
        domain=storage_share.uri['domain']
    )

c = boto3.client(
      's3',
      region_name=storage_share.plugin_settings['s3.region'],
      endpoint_url=_api_url,
      aws_access_key_id=storage_share.plugin_settings['s3.pub_key'],
      aws_secret_access_key=storage_share.plugin_settings['s3.priv_key'],
      use_ssl=True,
      verify=storage_share.plugin_settings['ssl_check'],
      config=Config(
          signature_version=storage_share.plugin_settings['s3.signature_ver'],
          connect_timeout=int(storage_share.plugin_settings['conn_timeout']),
          retries=dict(max_attempts=0)
      ),
  )
key = 'ztest'
bucket = storage_share.uri['bucket']

result = c.head_object(Bucket=bucket, Key=key)
#print("\nchale: %s\n" % sys.argv)
if sys.argv[2] == 'adler32':
    print( ">>>>> HASH %s\n" % result['Metadata']['digest-adler32'])
elif sys.argv[2] == 'md5':
    print( ">>>>> HASH %s\n" % result['Metadata']['digest-md5'])
