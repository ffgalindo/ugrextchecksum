import rados, json, sys

pool='rbd'

try:
    cluster = rados.Rados(conffile='/etc/ceph/ceph.conf', conf = dict (keyring = '/etc/ceph/ceph.client.admin.keyring'))
except:
    print("Error reading Ceph configuration.")
    exit(1)

print("\nlibrados version: " + str(cluster.version()))
print ("Will attempt to connect to: " + str(cluster.conf_get('mon initial members')))


try:
    cluster.connect()
except:
    print("Error connecting to Ceph Cluster")
    exit(1)

print ("\nCluster ID: " + cluster.get_fsid())
print ("\n\nCluster Statistics")
print ("==================")

cluster_stats = cluster.get_cluster_stats()

for key, value in cluster_stats.items():
    print (key, value)

try:
    print("\nOpening pool: %s" % (pool))
    ioctx = cluster.open_ioctx(pool)
except:
    print("\nError opening pool")
    cluster.shutdown()
    exit(1)

cmd = {
    "script": """
        function adler32(input, output)
          byte = string.byte
          size = objclass.stat()
          data = objclass.read(0, size)
          prime = 65521
          s1, s2 = 1, 0
          if #data > (1 << 40) then error("adler32: string too large") end
          for i = 1,#data do
            b = byte(data, i)
          	s1 = s1 + b
    		s2 = s2 + s1
          end
          s1 = s1 % prime
          s2 = s2 % prime
          return (s2 << 16) + s1
        end
        objclass.register(adler32)
    """,
    "handler": "adler32",
}

try:
    print("\nCalculating checksum of object: %s" % (str(sys.argv[1])))
    ret, data = ioctx.execute(str(sys.argv[1]), 'lua', 'eval_json', json.dumps(cmd).encode('utf_8'))
    print("\n%s" % (data[:ret].decode()))
except:
    print("\nFailed to read object.")
    cluster.shutdown()
    exit(1)

ioctx.close()
cluster.shutdown()
