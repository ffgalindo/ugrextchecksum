#!/bin/python3

""" Script to request checksum of a particular file to a remote SSH host.

Arguments:
$1 -- UGR's endpoint ID.
$2 -- UGR's config file containing ID.
$3 -- Signed GET URL
$4 -- Checksum Type (md5, adler32, crc32)

Returns:
String to stdout with the format (no quotations):
">>>>> HASH 123124\n"
"""

import logging, logging.handlers
import os
import re
import sys
import subprocess
from urllib.parse import urlsplit, urlunsplit

#############
# Functions #
#############

def get_remote_checksum(cmd, filepath, hash_type, host):
    """Connect to remote host via SSH and run command to obtain file checksum.

    Arguments:
    cmd -- path to script to execute on remote host.
    filepath -- path to file for which we wish to obtain the checksum.
    hash_type -- Type of hash to calculate.
    host -- address of remote SSH host

    Returns:
    Tuple of strings checksum, stdout, stderr from remote script.

    """
    # Creating logger
    _logger = logging.getLogger(__name__)

    _logger.info(
        'Running checksum script on remote host: %s',
        host,
    )
    _logger.debug(
        'SSH command: "ssh %s %s %s %s"',
        host,
        cmd,
        filepath,
        hash_type
    )

    _process = subprocess.Popen(
        ['ssh', host, cmd, filepath, hash_type],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT
    )
    _stdout, _stderr = _process.communicate()

    if _stdout is not None:
        _stdout = _stdout.decode("utf-8")

    if _stderr is not None:
        _stderr = _stderr.decode("utf-8")

    _checksum = _stdout.splitlines()[0].split()[3]

    # adler32 checksums need to be 8 characters. If not, prefix with 0's.
    if hash_type == 'adler32':
        _checksum = _checksum.rjust(8, '0')

    _checksum = validate_checksum(
        _checksum.rstrip('\n'),
        hash_type
    )

    return (_checksum, _stdout, _stderr)

def get_dynafed_storage_checksum(config_file, endpoint, hash_type, url):
    """Call dynafed_storage checksums get command to obtain file checksum.

    Arguments:
    config_file -- path to UGR's cofiguration file of the requested endpoint.
    endpoint -- UGR id of endpoint containing the requested object.
    hash_type -- Type of hash to calculate.
    url -- URL of the object/file to request.

    Returns:
    Tuple of strings checksum, stdout, stderr from remote script.

    """
    # Creating logger
    _logger = logging.getLogger(__name__)

    _logger.info(
        'Running script: /usr/local/bin/dynafed-storage checksums get -c %s -e %s -t %s -u %s',
        config_file,
        endpoint,
        hash_type,
        url
    )

    _process = subprocess.Popen(
        ['/usr/local/bin/dynafed-storage', 'checksums', 'get',
         '-c', config_file,
         '-e', endpoint,
         '-t', hash_type,
         '-u', url
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT
    )
    _stdout, _stderr = _process.communicate()

    if _stdout is not None:
        _stdout = _stdout.decode("utf-8")

    if _stderr is not None:
        _stderr = _stderr.decode("utf-8")

    _checksum = _stdout

    # adler32 checksums need to be 8 characters. If not, prefix with 0's.
    if hash_type == 'adler32':
        _checksum = _checksum.rjust(8, '0')

    _checksum = validate_checksum(
        _checksum.rstrip('\n'),
        hash_type
    )

    return (_checksum, _stdout, _stderr)

def put_dynafed_storage_checksum(checksum, config_file, endpoint, hash_type, url):
    """Call dynafed_storage checksums put command to upload file checksum.

    Arguments:
    checksum -- string with checksum to upload.
    config_file -- path to UGR's cofiguration file of the requested endpoint.
    endpoint -- UGR id of endpoint containing the requested object.
    hash_type -- Type of hash to calculate.
    url -- URL of the object/file to request.

    """
    # Creating logger
    _logger = logging.getLogger(__name__)

    _logger.info(
        'Running script: /usr/local/bin/dynafed-storage checksums put --checksum %s -c %s -e %s -t %s -u %s',
        checksum,
        config_file,
        endpoint,
        hash_type,
        url
    )

    _process = subprocess.Popen(
        ['/usr/local/bin/dynafed-storage', 'checksums', 'put',
         '--logfile=/var/log/dynafed_storagestats/checksum.log',
         '--loglevel=DEBUG',
         '--checksum', checksum,
         '-c', config_file,
         '-e', endpoint,
         '-t', hash_type,
         '-u', url
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT
    )
    _stdout, _stderr = _process.communicate()

    if _stdout is not None:
        _stdout = _stdout.decode("utf-8")

    if _stderr is not None:
        _stderr = _stderr.decode("utf-8")

def setup_logger(logfile="/tmp/checksum.log", loglevel="WARNING", verbose=False):
    """Setup the logger format to be used throughout the script.

    Arguments:
    logfile -- string defining path to write logs to.
    loglevel -- string defining level to log: "DEBUG, INFO, WARNING, ERROR"
    verbose -- boolean. 'True' prints log messages to stderr.

    Returns:
    logging.Logger object.

    """
    # To capture warnings emitted by modules.
    logging.captureWarnings(True)

    # Create file logger.
    _logger = logging.getLogger(__name__)

    # Set log level to use.
    _num_loglevel = getattr(logging, loglevel.upper())
    _logger.setLevel(_num_loglevel)

    # Set logger format
    _log_format_file = logging.Formatter('%(asctime)s - [%(levelname)s]%(message)s')

    # Set file where to log and the mode to use and set the format to use.
    _log_handler_file = logging.handlers.TimedRotatingFileHandler(
        logfile,
        when="midnight",
        backupCount=15,
    )

    _log_handler_file.setFormatter(_log_format_file)

    # Add the file handler created above.
    _logger.addHandler(_log_handler_file)

    # Create STDERR hanler if verbose is requested and add it to logger.
    if verbose:
        log_format_stderr = logging.Formatter('%(asctime)s - [%(levelname)s]%(message)s')
        log_handler_stderr = logging.StreamHandler()
        log_handler_stderr.setLevel(_num_loglevel)
        log_handler_stderr.setFormatter(log_format_stderr)
        # Add handler
        _logger.addHandler(log_handler_stderr)

    return _logger

def validate_checksum(checksum, hash_type):
    """Check that the checksum script matches the format of the requested hash.

    Arguments:
    hash_type: Type of checksum requested.

    Returns:
    Either string checksum or 'Error'

    """
    # Creating logger
    _logger = logging.getLogger(__name__)

    _validators = {
        'adler32': {
            'empty_file_checksum': '00000001',
            'length': 8,
            'pattern': '([a-fA-F\d]{8})'
        },
        'md5': {
            'empty_file_checksum': 'd41d8cd98f00b204e9800998ecf8427e',
            'length': 32,
            'pattern': '([a-fA-F\d]{32})'
        }
    }

    try:
        if (
            re.findall(_validators[hash_type]['pattern'], checksum)
            and checksum != _validators[hash_type]['empty_file_checksum']
        ):
            _logger.info(
                'Validating %s checksum: "%s"',
                hash_type,
                checksum
            )
            _logger.debug(
                'Validating checksum against pattern: "%s"',
                _validators[hash_type]['pattern'],
            )

            return checksum

        else:
            _logger.error(
                'Failed to validate %s checksum: "%s"',
                hash_type,
                checksum
            )

            return 'Error'

    except KeyError:
        _logger.error(
            'Unsupported checksum type: "%s"',
            hash_type
        )

        return 'Error'

#################
# Main Function #
#################

def main():
    """Main Function"""

    # Runtime Variables.
    ENDPOINT = sys.argv[1]
    CONFIG_FILE = sys.argv[2]
    URL = sys.argv[3]
    HASH_TYPE = sys.argv[4]
    MOUNT_POINT = '/buckets'
    SSH_HOST = 'root@atlas-fed-rgw01.triumf.ca'
    CMD = '/usr/local/bin/run_checksum.sh'

    # Environmental Variables.
    os.environ['PYTHONUNBUFFERED'] = '1'

    # Configure local script logger.
    setup_logger(
        logfile='/var/log/dynafed_storagestats/checksum.log',
        loglevel='DEBUG',
        verbose=True,
    )

    # Creating logger
    _logger = logging.getLogger(__name__)

    _url_parts = urlsplit(URL)

    # Get the URL without query or fragments (last two elements in array)
    _url_noquery = urlunsplit(
        (
         _url_parts.scheme,
         _url_parts.netloc,
         _url_parts.path,
         '',
         ''
        )
    )

    _logger.info(
        "[%s]ugrextchecksum_log: "
    )

    _checksum, _stdout, _stderr = get_dynafed_storage_checksum(
        CONFIG_FILE,
        ENDPOINT,
        HASH_TYPE,
        _url_noquery
    )

    if _checksum == 'Error':

        filepath = MOUNT_POINT + _url_parts.path

        _checksum, _stdout, _stderr = get_remote_checksum(
            CMD,
            filepath,
            HASH_TYPE,
            SSH_HOST
        )

        _logger.info(
            "[%s]ugrextchecksum_log: stdout: '%s', stderr: '%s'",
            ENDPOINT,
            _stdout,
            _stderr,
        )

    if _checksum != 'Error':
        _logger.info(
            "[%s]Checksum %s: '%s'",
            ENDPOINT,
            HASH_TYPE,
            _checksum.rstrip('\n'),
        )

        put_dynafed_storage_checksum(
            _checksum,
            CONFIG_FILE,
            ENDPOINT,
            HASH_TYPE,
            URL
        )

        print(">>>>> HASH %s\n" % (_checksum))


#############
# Self-Test #
#############

if __name__ == '__main__':
    main()
