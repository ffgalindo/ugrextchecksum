#!/bin/bash

# Substitute this extermely complex calculation
# with the one that best suits your backend
#
# Input arguments:
# $1 Signed GET URL
# $2 Checksum Type (md5, adler32)
#
# Output:
# Should stdout with the format (no quotations):
# ">>>>> HASH 123124\n"

OUTPUT=$(mktemp)
URL=$1
HASH_TYPE=$2


curl -k $URL -o $OUTPUT 2> /dev/null

case $HASH_TYPE in
  adler32)
    echo -e ">>>>> HASH `gfal-sum $OUTPUT adler32 |  awk '{print $2}'`\n"
    ;;

  md5)
    echo -e ">>>>> HASH `gfal-sum $OUTPUT md5 |  awk '{print $2}'`\n"
    ;;

  *)
    echo "Not a recognized digest type given: $HASH_TYPE"
    ;;

esac

#cleanup
rm -f $OUTPUT
