import rados, json, sys

pool='default.rgw.buckets.data'
#bucket_id='b2666cef-58b3-488e-8f60-33a9f08be373.1089719.1'

try:
    cluster = rados.Rados(conffile='/etc/ceph/ceph.conf', conf = dict (keyring = '/etc/ceph/ceph.client.admin.keyring'))
except:
    print("Error reading Ceph configuration.")
    exit(1)

print("\nlibrados version: " + str(cluster.version()))
print ("Will attempt to connect to: " + str(cluster.conf_get('mon initial members')))


try:
    cluster.connect()
except:
    print("Error connecting to Ceph Cluster")
    exit(1)

print ("\nCluster ID: " + cluster.get_fsid())
print ("\n\nCluster Statistics")
print ("==================")

cluster_stats = cluster.get_cluster_stats()

for key, value in cluster_stats.items():
    print (key, value)

try:
    print("\nOpening pool: %s" % (pool))
    ioctx = cluster.open_ioctx(pool)
except:
    print("\nError opening pool")
    cluster.shutdown()
    exit(1)
if sys.argv[1] == "adler32":
    cmd = {
        "script": """
            function compute_adler32(input, output)
                size = objclass.stat()
                data = objclass.read(0, size)
                local s = data:str()
                local prime = 65521
                local s1 = 1
                local s2 = 0
                if #s > (1 << 40) then error("adler32: string too large") end
                for i = 1,#s do
                    local b = string.byte(s, i)
                	s1 = s1 + b
          		    s2 = s2 + s1
                    s1 = s1 % prime
                    s2 = s2 % prime
                end
                local digest = (s2 << 16) + s1
                output:append(string.format("%08x", digest))
            end
            objclass.register(compute_adler32)
        """,
        "handler": "compute_adler32",
    }
    ret, data = ioctx.execute(str(sys.argv[2]), 'lua', 'eval_json', json.dumps(cmd).encode('utf_8'))
    print("\n%s" % (data))
    ioctx.close()
    cluster.shutdown()

elif sys.argv[1] == "md5":
    # cmd = {
    #     "script": """
    #         function compute_md5(input, output)
    #             md5 = require 'md5’
    #             size = objclass.stat()
    #             data = objclass.read(0, size)
    #             myOut = md5.sumhexa(data)
    #             type = type(myOut)
    #             output:append(type)
    #         end
    #         objclass.register(compute_md5)
    #     """,
    #     "handler": "compute_md5",
    # }
    ret, data = ioctx.execute(str(sys.argv[2]), 'md5', 'calc_md5', b'')
    print("\n%s" % (data))
    ioctx.close()
    cluster.shutdown()

# try:
#     print("\nCalculating checksum of object: %s" % (str(sys.argv[2])))
#     ret, data = ioctx.execute(str(sys.argv[2]), 'lua', 'eval_json', json.dumps(cmd).encode('utf_8'))
#     print("\n%s" % (data[:ret].decode()))
# except:
#     print("\nFailed to read object.")
#     cluster.shutdown()
#     exit(1)
