#!/bin/bash
file=$1
HASH_TYPE=$2
lockfile=/tmp/mylock.$(basename $file)
CHECKSUM_LOG=/var/log/ugr/checksum.log
INFO_LOG=/var/log/ugr/checksum_info.log

if ( set -o noclobber; echo "$$" > "$lockfile") 2> /dev/null; then

        trap 'rm -f "$lockfile"; exit $?' INT TERM EXIT

        # do stuff here
        echo "[$(date --rfc-3339=seconds)]: Checksumming $file" >> $INFO_LOG
        echo "[$(date --rfc-3339=seconds)]: $(gfal-sum $file $HASH_TYPE)" >> $CHECKSUM_LOG

        # clean up after yourself, and release your trap
        rm -f "$lockfile"
        trap - INT TERM EXIT
else
        echo "[$(date --rfc-3339=seconds)]: Lock Exists: $lockfile owned by $(cat $lockfile). Waiting to steal it's output." >> $INFO_LOG
        _PID=$(cat $lockfile)
        while [ -e /proc/$_PID ]; do sleep 0.1; done
        echo "[$(date --rfc-3339=seconds)]: Stealing it!" >> $INFO_LOG
fi

echo `grep $file $CHECKSUM_LOG | tail -n 1`
