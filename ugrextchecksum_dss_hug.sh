#!/bin/bash

# Substitute this extermely complex calculation
# with the one that best suits your backend
#
# Input arguments:
# $1 Signed GET URL
# $2 Checksum Type (md5, adler32, crc32)
#
# Output:
# Should stdout with the format (no quotations):
# ">>>>> HASH 123124\n"

API='localhost:8000'
ENDPOINT=$1
CONFIG_FILE=$2
URL=$3
HASH_TYPE=$4

checksum=$(dynafed-storage checksums get -c $CONFIG_FILE -e $ENDPOINT -t $HASH_TYPE -u $URL)
if [ $checksum != 'None' ]
  then
    #Strip leading and trailing quotes.
    checksum=$(sed -e 's/^"//' -e 's/"$//' <<<"$checksum")
    echo -e ">>>>> HASH $checksum\n"
else
  checksum=$(curl -k -XPOST $API/$HASH_TYPE --data-urlencode url=$URL)
  #Strip leading and trailing quotes.
  checksum=$(sed -e 's/^"//' -e 's/"$//' <<<"$checksum")
  #Upload checksum to object's metadata, if we get a valid checksum.
  if [ $checksum != '00000001' ] && [ $checksum != 'd41d8cd98f00b204e9800998ecf8427e' ]
  then
    dynafed-storage checksums put -c $CONFIG_FILE -e $ENDPOINT -t $HASH_TYPE -u $URL --checksum $checksum
  fi
  echo -e ">>>>> HASH $checksum\n"
fi
